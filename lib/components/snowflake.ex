defmodule Coffee.Snowflake do
  use Scenic.Component, has_children: false
  import Scenic.Primitives, only: [line: 3, update_opts: 2]
  alias Scenic.Graph
  alias Graphmath.Vec2

  @graph Graph.build(font: :roboto, font_size: 24)
  @sixty_degrees -60 * :math.pi() / 180
  @frame_ms 20
  @frames_per_level 750
  @max_levels 4

  def verify(level: level, width: width, height: height)
      when level >= 0 and width >= 0 and height >= 0,
      do: {:ok, [level: level, width: width, height: height]}

  def verify(_), do: :invalid_data

  def info(data),
    do: """
         #{IO.ANSI.red()}#{__MODULE__} data must be an integer, 0 or higher
         #{IO.ANSI.yellow()}Received: #{inspect(data)}
         #{IO.ANSI.default_color()}
    """

  def init([level: level, width: width, height: height], opts) do
    base_graph = Graph.modify(@graph, :_root, &update_opts(&1, styles: opts[:styles]))

    snowflake = generate_snowflake(level, {width, height})

    graph = draw_snowflake(snowflake, base_graph)

    {:ok, timer} = :timer.send_interval(@frame_ms, :frame)

    state = %{
      graph: graph,
      base_graph: base_graph,
      level: level,
      snowflake: snowflake,
      timer: timer,
      frame: 1,
      width: width,
      height: height
    }

    {:ok, state, push: graph}
  end

  def handle_info(
        :frame,
        %{
          base_graph: graph,
          level: level,
          frame: frame,
          width: width,
          height: height
        } = state
      )
      when rem(frame, @frames_per_level) == 0 and level == @max_levels do
    snowflake = generate_snowflake(0, {width, height})

    graph = draw_snowflake(snowflake, graph)

    {:noreply, %{state | snowflake: snowflake, level: 0, frame: frame + 1, graph: graph},
     push: graph}
  end

  def handle_info(
        :frame,
        %{
          base_graph: graph,
          level: level,
          snowflake: snowflake,
          frame: frame
        } = state
      )
      when rem(frame, @frames_per_level) == 0 do
    snowflake = add_level(snowflake)

    graph = draw_snowflake(snowflake, graph)

    {:noreply,
     %{
       state
       | snowflake: snowflake,
         level: level + 1,
         frame: frame + 1,
         graph: graph
     }, push: graph}
  end

  def handle_info(
        :frame,
        %{graph: graph, frame: frame} = state
      ) do
    {:noreply, %{state | frame: frame + 1}, push: graph}
  end

  def draw_snowflake([_], graph), do: graph

  def draw_snowflake([from, to | rest], graph),
    do: draw_snowflake([to | rest], line(graph, {from, to}, stroke: {4, :white}, id: :snowflake))

  defp generate_snowflake(0, {width, height}),
    do: [{0, height}, {div(width, 2), 0}, {width, height}, {0, height}]

  defp generate_snowflake(i, {width, height}) do
    generate_snowflake(i - 1, {width, height})
    |> add_level()
  end

  defp add_level(snowflake) do
    List.foldr(snowflake, [], fn
      x, [] ->
        [x]

      x, [y | rest] ->
        List.flatten([x, points(x, y), y | rest])
    end)
  end

  defp points(start_point, end_point) do
    i_vec = Vec2.subtract(end_point, start_point)

    first_third =
      i_vec
      |> Vec2.scale(1 / 3)
      |> Vec2.add(start_point)

    point =
      first_third
      |> Vec2.add(Vec2.rotate(Vec2.scale(i_vec, 1 / 3), @sixty_degrees))

    last_third =
      i_vec
      |> Vec2.scale(2 / 3)
      |> Vec2.add(start_point)

    [first_third, point, last_third]
  end
end
