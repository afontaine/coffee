defmodule Coffee.Tracker do
  use Scenic.Component, has_children: false
  import Scenic.Primitives, only: [text: 3, update_opts: 2]
  import Coffee.Gettext
  alias Scenic.Graph

  @graph Graph.build(font: :roboto, font_size: 24)
  @frame_ms 20

  def verify(%NaiveDateTime{} = x), do: {:ok, x}

  def info(data),
    do: """
         #{IO.ANSI.red()}#{__MODULE__} data must be a naive date time
         #{IO.ANSI.yellow()}Received: #{inspect(data)}
         #{IO.ANSI.default_color()}
    """

  def init(time, opts) do
    {:ok, timer} = :timer.send_interval(@frame_ms, :frame)

    graph =
      @graph
      |> Graph.modify(:_root, &update_opts(&1, styles: opts[:styles]))
      |> get_text(time)

    {:ok, %{graph: graph, start: time, timer: timer}, push: graph}
  end

  def handle_info(:frame, %{graph: graph, start: start} = state) do
    graph = Graph.modify(graph, :tracker, &get_text(&1, start))
    {:noreply, %{state | graph: graph}, push: graph}
  end

  def get_text(graph, start) do
    case(NaiveDateTime.diff(NaiveDateTime.utc_now(), start)) do
      i when i < 60 ->
        text(
          graph,
          ngettext(
            """
              It has been
              %{seconds} second
              since your last coffee
            """,
            """
              It has been
              %{seconds} seconds
              since your last coffee
            """,
            i,
            seconds: i
          ),
          id: :tracker
        )

      i when i < 3600 ->
        text(
          graph,
          ngettext(
            """
              It has been
              %{minutes} minute
              since your last coffee
            """,
            """
              It has been
              %{minutes} minutes
              since your last coffee
            """,
            div(i, 60),
            minutes: div(i, 60)
          ),
          id: :tracker
        )

      i when i < 86400 ->
        text(
          graph,
          ngettext(
            """
              It has been
              %{hours} hour
              since your last coffee
            """,
            """
              It has been
              %{hours} hours
              since your last coffee
            """,
            div(i, 3600),
            hours: div(i, 3600)
          ),
          id: :tracker
        )
    end
  end
end
