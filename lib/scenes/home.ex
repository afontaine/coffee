defmodule Coffee.Scene.Home do
  use Scenic.Scene

  alias Scenic.Graph
  alias Scenic.ViewPort

  @text_size 24

  # ============================================================================
  # setup

  # --------------------------------------------------------
  def init(_, opts) do
    # get the width and height of the viewport. This is to demonstrate creating
    # a transparent full-screen rectangle to catch user input
    {:ok, %ViewPort.Status{size: {width, height}}} = ViewPort.info(opts[:viewport])

    graph =
      Graph.build(font: :roboto, font_size: @text_size)
      |> Coffee.Snowflake.add_to_graph(
        [
          level: 0,
          width: width,
          height: height
        ],
        translate: {(1 - 0.6) * div(width, 2), (1 - 0.6) * div(height, 4)},
        scale: 0.6
      )
      |> Coffee.Tracker.add_to_graph(NaiveDateTime.utc_now(),
        translate: {div(width, 2), div(height, 2)},
        text_align: :center
      )

    {:ok, graph, push: graph}
  end
end
