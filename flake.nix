{
  description = "Magic Mirror built with Phoenix LiveView and Nerves";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix-elixir = {
    url = "github:hauleth/nix-elixir";
    flake = false;
  };

  outputs = { self, nixpkgs, flake-utils, nix-elixir }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ (import nix-elixir) ];
        };
        erlangPackages = pkgs.beam.packagesWith pkgs.beam.interpreters.erlang;
        elixir = erlangPackages.elixir_1_11;
        erlang = erlangPackages.erlang;
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = [
            elixir
            pkgs.pkgconfig
            pkgs.glfw
            pkgs.glew
            erlangPackages.elixir-ls
          ] ++ pkgs.lib.optionals pkgs.stdenv.isLinux [
            pkgs.x11
            pkgs.xorg.libpthreadstubs
            pkgs.xorg.libXcursor
            pkgs.xorg.libXdmcp
            pkgs.xorg.libXfixes
            pkgs.xorg.libXinerama
            pkgs.xorg.libXrandr
            pkgs.xorg.xrandr
          ] ++ pkgs.lib.optionals pkgs.stdenv.isDarwin
            [ pkgs.darwin.apple_sdk.frameworks.Cocoa ];
        };
        packages = flake-utils.lib.flattenTree {
          coffee = erlangPackages.buildMix' {
            pname = "coffee";
            version = "v1.0.0";
            src = ./.;
            nativeBuildInputs = [ pkgs.pkgconfig pkgs.glfw pkgs.glew ]
              ++ pkgs.lib.optionals pkgs.stdenv.isLinux [
                pkgs.x11
                pkgs.xorg.libpthreadstubs
                pkgs.xorg.libXcursor
                pkgs.xorg.libXdmcp
                pkgs.xorg.libXfixes
                pkgs.xorg.libXinerama
                pkgs.xorg.libXrandr
                pkgs.xorg.xrandr
              ] ++ pkgs.lib.optionals pkgs.stdenv.isDarwin
              [ pkgs.darwin.apple_sdk.frameworks.Cocoa ];
            buildPhase = ''
              runHook preBuild
              mix do compile --no-deps-check, release
            '';
            installPhase = ''
              runHook preInstall
              mkdir -p $out/bin
              mv _build/prod/rel/bakeware/coffee $out/bin
              runHook postInstall
            '';
          };
        };
        defaultPackage = packages.coffee;
        apps.coffee = flake-utils.lib.mkApp { drv = packages.coffee; };
        defaultApp = apps.coffee;
      });
}
