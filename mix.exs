defmodule Coffee.MixProject do
  use Mix.Project

  def project do
    [
      app: :coffee,
      version: "1.0.0",
      elixir: "~> 1.7",
      build_embedded: true,
      start_permanent: Mix.env() == :prod,
      releases: [{:coffee, release()}],
      deps: deps(),
      preferred_cli_env: [release: :prod]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Coffee, []},
      extra_applications: [:crypto]
    ]
  end

  defp release do
    [
      overwrite: true,
      cookie: "coffee_cookie",
      steps: [:assemble, &Bakeware.assemble/1],
      strip_beams: Mix.env() == :prod
    ]
  end
  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:scenic, "~> 0.10"},
      {:scenic_driver_glfw, "~> 0.10", targets: :host},
      {:graphmath, "~> 2.4"},
      {:gettext, "~> 0.18.2"},
      {:bakeware, "~> 0.1.4", runtime: false}
    ]
  end
end
