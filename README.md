# Coffee

A (very simple) timer that tells you how long since your last coffee.

## Building and Running

### I am cool and I have `nix` installed

`nix run gitlab:afontaine/coffee`

### I don't have nix installed

1. Install [`scenic`'s dependencies](https://hexdocs.pm/scenic/install_dependencies.html)
1. Install this projects dependencies
   ```
   $ mix deps.get
   ```
1. Run it or build it
   ```
   # to run
   $ mix scenic.run
   # to build release, bakeware executable available at _build/prod/rel/bakeware
   $ mix release
   ```
